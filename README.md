# There are two options to run this:

# 1. As a single process

* cd src
* javac *.java 
* java Stepping -i s0.in 

# 2. In network mode 

* Open two different terminals and navigate to the src directiory 
* java Stepping -p 31415 -i s0.in
* Type this in the other window:
* java Agent -p 31415

Note 31415 is the port number and can be interchanged with any other valid port if port 31415 is unavailable