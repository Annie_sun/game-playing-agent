/**
 * Tile class: <br>
 * -Represents each individual tile in the world <br>
 * -Uses x and y co-ordinates to denote its location on the whole map <br>
 * - x and y can be negative values <br>
 * - (0,0) is the start location and its item is represented by H (for home) <br>
 * - Has functions to rotate the tile itself left or right depending on the action taken
 *
 * @author Annie Sun (z5075255)
 */

public class Tile {
	/**
	 * Constructs a Tile object 
	 * @param x      x coordinate
	 * @param y      y coordinate
	 * @param item   item type
	 */
	public Tile(int x, int y, char item){
		this.x = x;
		this.y = y;
		this.item = item;
	}
	
	/**
	 * Rotates the tile to the left 
	 */
	public void rotateLeft() {
		int tempX, tempY; 
		tempX = x;
		tempY = y;
		
		x = tempY;
		y = - tempX;	
	}
	
	/**
	 * Rotates the tile to the right
	 */
	public void rotateRight () {
		int tempX, tempY;
		tempX = x;
		tempY = y;
		
		x = -tempY;
		y = tempX;
	}
	
	/**
	 * Moves the tile co-ordinates by X and Y
	 * @param X  number to move the x coordinate by
	 * @param Y  number to move the y coordinate by
	 */
	public void translate(int X, int Y){
		x += X;
		y += Y;
	}
	
	/**
	 * Returns the Manhattan distance as an int
	 * @return manhattan distance
	 */
	public int manDist() {
		int value = 0;
		int xValue = absoluteValue(x);
		int yValue = absoluteValue(y);
 
		value = xValue + yValue;
		
		return value;
	}
	
	/**
	 * Private helper method to calculate the absolute value of a num
	 * @param num   any number
	 * @return      absolute value of the number
	 */
	private int absoluteValue (int num) {
		int value = 0;
		
		if (num > 0) {
			value = num;
		} else {
			value = -num;
		}
		return value;
	}
	
	/**
	 * Returns the x coordinate of this tile
	 * @return  x coordinate
	 */
	public int getX(){
		return this.x;
	}

	/**
	 * Returns the y coordinate of this tile
	 * @return  y coordinate
	 */
	public int getY(){
		return this.y;
	}

	/**
	 * Returns the character representing the item on this tile (eg. axe, water, door)
	 * @return  item/obstacle/status of this tile
	 */
	public char getItem(){
		return this.item;
	}
	
	/**
	 * Clones this tile object
	 * @ return clone copy of this tile object
	 */
	public Tile clone(){
		Tile clone = new Tile(this.x, this.y, this.item);
		return clone;
	}
	
	/**
	 * Checks whether two Tiles are identical to each other
	 * @return boolean value indicating whether this is identical to a compared object
	 */
	public boolean equals(Object other){
		if(other != null && other.getClass() == this.getClass()){
			Tile o = (Tile) other;
			if(o.getX() == this.x && o.getY() == this.y && o.getItem() == this.item){
				return true;
			}
		}
		return false;
	}
	
	private int x;
	private int y;
	private char item;
}