import java.util.*;
import java.io.*;
import java.net.*;

/**
 * Agent class: <br>
 * - Contains main function <br>
 * - Sends relevant data to methods in other classes <br>
 * - Returns a character depicting an action <br>
 * - Double checks whether the action won't lead to a death <br> <br>
 *
 * <u> Agent.java was created with the help of the sample code in zip file. Credits goes
 * to Alan Blair (COMP3411 16s1)  </u><br><b>
 * <br>
 * 
 * <p> Answer to the question: </b> <br>
 * 
 * How the program works: (basic outline)  <ol>
 * <li> Updates the world map according to 5x5 window view 
 * <li> Starts an a* search with a given state
 * <li> Creates a new priority queue and a hashmap of parent and child states
 * <li> Push given state onto the queue 
 * <li> Evaluate lowest heuristic value of state
 * <li> Check next states and add them to the queue (only if valid states)
 * <li> Repeat 5-6 until queue is empty. 
 * <li> Change list of states into string format
 * <li> If the action is valid, then implement the action
 * <li> Update the map/item values according to the action taken
 * <li> Do this until there is no more actions left in the string
 * <li> Repeat whole process until map is cleared
 * <br><br>
 * This program uses A*search of states as its algorithm. The heuristic priorities 
 * certain actions and if there is none of these actions available, it determines 
 * the distance from agent to item multiplied by a weight. (more details in heuristic.java).
 * Data structures used are mainly arrayLists and hashMaps. Though arraylists can be 
 * interchanged with linked lists. Originally, I was planning to separate state into two different 
 * classes: tools and map. However, since the two are very closely interdependent, it was more convenient 
 * to combine the two together into a single class (state). A list of tile objects (representing the world map) 
 * is used as it is easier to manipulate compared to a 2D array. 
 * <br>
 *  @author Annie Sun (z5075255)
 *
 */

public class Agent {	
	/**
	 * Constructs an Agent object
	 */
	public Agent(){
		this.game = new State();
	}
	
	/**
	 * Returns an action (char) back to the game engine after the AI goes through a search
	 * @param view    5 x 5 window view of the AI
	 * @return        a single character depicting a certain action (either f, l, r, c, u) 
	 */
	public char get_action(char view[][]) {
		
        game.setPrevious(game.clone());
        game.updateMap(view);
        AStarSearch search = new AStarSearch(game);
        
		if(actionList.length() == 0){
			
			search.search();
			List<State> moves = search.getMoves();
			
			actionList = "";
            int lastTurn = 0;
            int counter = 0;
			for(State state: moves){
                char lastMove = state.getLastMove();
				actionList += lastMove;
                if(lastMove != 'l' && lastMove != 'r' || lastMove != 'R' || lastMove != 'L'){
                    lastTurn = counter;
                }
                counter++;
			}
			
			actionList = actionList.substring(1, lastTurn+1);
		}
		
		// in cases where the a* star search doesn't return any possible actions taken
		if(actionList.length() == 0){			
            actionList = "flfrfl";
           
        } 
	
		char action = actionList.charAt(0);
	    actionList = actionList.substring(1);
		
        String possibleMoves = game.possibleMoves();
        
        if(possibleMoves.indexOf(action) == INVALID){  	
            return get_action(view);
        }       
        game.demo(action);
      
		return action;
	}
	
	
	void print_view( char view[][] )
	   {
	      int i,j;

	      System.out.println("\n+-----+");
	      for( i=0; i < 5; i++ ) {
	         System.out.print("|");
	         for( j=0; j < 5; j++ ) {
	            if(( i == 2 )&&( j == 2 )) {
	               System.out.print('^');
	            }
	            else {
	               System.out.print( view[i][j] );
	            }
	         }
	         System.out.println("|");
	      }
	      System.out.println("+-----+");
	   }
	
	public static void main( String[] args ){
		InputStream in  = null;
		OutputStream out= null;
		Socket socket   = null;
		Agent  agent    = new Agent();
		int port;
		
		if( args.length < 2 ) {
			System.out.println("Usage: java Agent -p <port>\n");
			System.exit(-1);
		}
		
		port = Integer.parseInt( args[1] );
		
		try { // open socket to Game Engine
			socket = new Socket( "localhost", port );
			in  = socket.getInputStream();
			out = socket.getOutputStream();
		}catch( IOException e ) {
			System.out.println("Could not bind to port: "+port);
			System.exit(-1);
		}

		try { // scan 5-by-5 window around current location
			while( true ) {
				char   view[][] = new char[5][5];
				int i,j;
				for( i=0; i < 5; i++ ) {
					for( j=0; j < 5; j++ ) {
						if( !(( i == 2 )&&( j == 2 ))) {
							int ch = in.read();
							if( ch == -1 ) {
								
								System.exit(-1);
							}
							view[i][j] = (char) ch;
						}
					}
				}
				//agent.print_view( view ); 
				char action = agent.get_action( view );
				out.write( action );
				}
			}catch( IOException e ) {
				System.out.println("Lost connection to port: "+ port );
				System.exit(-1);
			}
		finally {
			try {
				socket.close();
			}catch( IOException e ) {
				e.printStackTrace();
			}
		}
	}
	
	private State game;
	private String actionList = "";
	
	private static final int INVALID = -1;
	
}