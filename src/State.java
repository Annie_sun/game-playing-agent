import java.util.ArrayList;
import java.util.HashMap;

/**
 * State class: <br>
 * - Stores all information relating to the current turn of a game <br>
 * - Includes the map of the world, updating the map according to action taken as well 
 * as current inventory status (key, gold, stones, axe)  <br>
 * - Map is essentially an arraylist of tiles according to their x, y co-ordinate 
 * (Note: a linked list could have been used instead, doesn't change the functionality)
 * - Responsible for changing the orientation of the map according to the direction 
 *  the agent is facing <br>
 * - Hashmap of the distance between agent and closest corresponding of items for AStarSearch 
 * class to use <br>
 * - Checks for possible moves in this game state 
 * 
 * @author Annie Sun (z5075255)
 *
 */
public class State {

	/**
	 * Constructs a State Object
	 */
	public State(){
		this.allTiles = new ArrayList<Tile>();
		this.startLocation = new Tile(0, 0, 'H');
		
	}
	
	/**
	 * Updates the whole map for the given 5x5 tiles 
	 * @param view   5 x 5 window view 
	 */
	public void updateMap (char view[][]){
		// each time the map is updates, it means one move have been made
		movesMade++;	
		for(int i=0; i < WINDOW_SIZE; i++ ) {
			for(int j=0; j < WINDOW_SIZE; j++ ) {
				updateTile(new Tile(i-2, j-2, view[j][i]));		
			}
		}	
	}

	/**
	 * Updates the specified individual tile
	 * @param newTile   tile to update
	 */
	private void updateTile(Tile newTile){
		
		boolean update = false;
		Tile temp = null;
		// checks if tile exists already
		for(Tile t : allTiles){
			if(t.getX() == newTile.getX() && t.getY() == newTile.getY()){
				temp = t;
				break;
			}
		}
		if (temp == null) {
			update = true;
		} else {
			update = false;
		}       
		if(temp != null && newTile.getItem() != '?'){
			allTiles.remove(temp);
			update = true;
			fillUnknowns(temp);
		}
		// if a new tile, then add it to the list of tiles
		if(update){
			if (newTile.getItem() == 'k') {
				keySeen = true;			
			} else if (newTile.getItem() == 'a') {
				axeSeen = true;
			} else if (newTile.getItem() == 'g') {
				goldSeen = true;
			}
			allTiles.add(newTile);
		}
		
	}

	// label the surrounding tiles as unknown if they don't have an item
	// (left, right, down, up)
	public void fillUnknowns(Tile t){
		char item = t.getItem();
		if(item != '.' && item != '?' ){
			updateTile(new Tile(t.getX()-1, t.getY(), '?'));
			updateTile(new Tile(t.getX()+1, t.getY(), '?'));
			updateTile(new Tile(t.getX(), t.getY()-1, '?'));
			updateTile(new Tile(t.getX(), t.getY()+1, '?'));
		}
	}
	
	/**
	 * Method that tries the selected action as well as updating the map if the move is possible <br>
	 * Eg. if door is unlocked, the corresponding tile is updated to an ' ' as door is open <br>
	 * OR if a move it not valid, then return false since it is not valid
	 * @param action   char value representing chosen action
	 * @return         boolean value indicating whether selected action is successful
	 */
	public boolean demo(char action){
		lastAction = action;
		fillUnknowns(getTile(0,0));

		if(( action == 'L' )||( action == 'l' )) {
			rotateMap('R');
			return true;
		}

		if(( action == 'R' )||( action == 'r' )) {
			rotateMap('L');
			return true;
		}

		inFront = getTile(0, -1).getItem();
		char frontItem = getTile(0, -1).getItem();
		if(( action == 'F' )||( action == 'f' )) {
			if(frontItem == '*' || frontItem == 'T' || frontItem == '-'){
				return true;
			}
			if(frontItem == '~' && stonesAvailable <= 0){
				return false;
			}

			if(stonesAvailable > 0 && frontItem == '~'){
				
				updateTile(new Tile(0,-1, 'O'));
				translateAll(0, 1);
				stonesAvailable--;
				
				return true;
			} else {
				translateAll(0, 1);
			}         

			// items in front that can be picked up
			if (frontItem == 'a') {
				axePickUp = true;
			} else if (frontItem == 'k') {
				keyPickUp = true;
			} else if (frontItem == 'g') {
				goldPickUp = true;
			} else if (frontItem == 'o') {
				stonesAvailable ++;
			}

			if( goldPickUp &&( startLocation.getX() == 0 && startLocation.getY() == 0)) {
				finished = true;
			}
			return true;
		}

		// chop tree if only axe is picked up
		if(action =='C' || action == 'c'){ 
			if ((frontItem == 'T') && axePickUp) {
				
				updateTile(new Tile(0,0, ' '));
			}
			return true;
		}

		// unlock door if only key is picked up
		if (action == 'U' || action == 'u'){ 

			if ((frontItem == '-') && keyPickUp) {
				
				updateTile(new Tile(0,0, ' '));
			}
			return true;
		}
		return false;
	}

	public String possibleMoves(){
		
		// Definitely not a possible move. Dead = dead for life + fail autotesting
		if (dead){
			return "";
		}

		Tile front = getTile(0, -1);
		String possibleMoves = "lr";  // turn left and right; might be new info to process

		char frontItem = front.getItem();
		
		if (frontItem != '?' && frontItem != 'T' && frontItem != '*' && frontItem != '.' 
				&& frontItem != '~' && frontItem != '-'){
			possibleMoves = 'f'+ possibleMoves;
		} else {    

			if(frontItem == 'T' && axePickUp){
				possibleMoves += 'c';
			} else if(frontItem == '-' && keyPickUp){
				possibleMoves += 'u';
			}  else if (goldSeen() && frontItem == '~' && (stonesAvailable > 0)) {
				possibleMoves += 'f';
			}
		}
		return possibleMoves;
	}
	
	/**
	 * Returns the tile of the specified coordinates
	 * @param x      x coordinate
	 * @param y      y coordinate
	 * @return      tile object of the x.y coordinates
	 */
	public Tile getTile(int x, int y){
		for(Tile t : allTiles){
			if(t.getX() == x && t.getY() == y){
				return t;
			}
		}
		// if not found, create a new one
		Tile notFound = new Tile(x, y, '.');
		return notFound;
	}

	/**
	 * Method that returns all the closest locations of a corresponding item (axe, key, etc)
	 * @return  a hashmap with the item (key) and closest location (tile) as its value
	 */
	public HashMap<Character, Tile> closestLocation() {
		HashMap<Character, Tile> result = new HashMap<Character, Tile>();
		for(Tile t : allTiles){
			Tile nearest = result.get(t.getItem());
			// closer than the one in the hashmap or nothing in there, then add/put in
			if(nearest == null || t.manDist() < nearest.manDist()){ 
				result.put(t.getItem(), t);
			}
		}
		return result;
	}

	/**
	 * Rotate map according to the specified action <br>
	 * Used in updating the tiles. 
	 * @param action  character representing a 'l' or 'r' action 
	 */
	public void rotateMap(char action) {
		
		if (action == 'L' || action == 'l') {
			if (orientation != NORTH) {
				orientation = orientation - 1;
			} else {
				orientation = WEST;
			}
			
			for (Tile t: allTiles) {
				t.rotateLeft();
			}
			startLocation.rotateLeft();
		} 

		if (action == 'R' || action == 'r') {
			if (orientation != WEST) {
				orientation = orientation + 1;
			} else {
				orientation = NORTH;
			}
			for (Tile t: allTiles) {
				t.rotateRight();
			}
			startLocation.rotateRight();
		}
	}

	/**
	 * Moves all the tiles by a certain x, y amount
	 * @param X   number to move the x coordinate by
	 * @param Y   number to move the y coordinate by
	 */
	public void translateAll(int X, int Y){
		startLocation.translate(X, Y);

		for(Tile t : allTiles){
			t.translate(X, Y);
		}
	}

	/**
	 * Returns the start location
	 * @return  tile object of start
	 */
	public Tile startLocation(){
		return this.startLocation;
	}

	/**
	 * Sets a state to be the parent of this state
	 * @param prev
	 */
	public void setPrevious(State prev){
		this.previous = prev;
	}

	/**
	 * Returns the parent of this state. <br>
	 * If state doesn't have a parent, then return itself as parent 
	 * @return   parent state of this state
	 */
	public State getPrevious(){
		if (this.previous == null) {
			return this;
		} else {
			return this.previous;   
		}
	}

	/**
	 * Returns whether the game is finished/won <br>
	 * Ie. Gold is picked up + back at the start tile
	 * @return  boolean value representing if the game has been won
	 */
	public boolean getWon(){
		return this.finished;
	}

	/**
	 * Returns whether the game is lost <br>
	 * Ie. If the agent falls into the water without a stepping stone
	 * @return   boolean value representing if the game has been lost
	 */
	public boolean getGameLost(){
		return this.dead;
	}
	
	
	/**
	 * Returns whether the gold has been picked up
	 * @return  boolean value representing if the gold has been picked up
	 */
	public boolean goldPickUp(){
		return this.goldPickUp;
	}

	/**
	 * Returns whether the axe has been picked up
	 * @return  boolean value representing if the axe has been picked up
	 */
	public boolean getAxePickUp(){
		return this.axePickUp;
	}

	/**
	 * Returns whether the key has been picked up
	 * @return  boolean value representing if the key has been picked up
	 */
	public boolean getKeyPickUp() {
		return this.keyPickUp;
	}
	
	/**
	 * Returns the number of stones still available to save you from drowning in the water
	 * @return  number of stones in inventory
	 */
	public int getStonesAvailable() {
		return this.stonesAvailable;
	}
	/**
	 * Returns whether a key has been seen on the map
	 * @return  boolean value of whether key has been seen
	 */
	public boolean keySeen() {
		return this.keySeen;
	}
	
	/**
	 * Returns whether an axe has been seen on the map
	 * @return  boolean value of whether key has been seen
	 */
	public boolean axeSeen() {
		return this.axeSeen;
	}
	
	/**
	 * Returns whether gold has been seen on the map
	 * @return  boolean value of whether gold has been seen
	 */
	public boolean goldSeen() {
		return this.goldSeen;
	}
	
	/**
	 * Returns the last move made
	 * @return  character representing last character made
	 */
	public char getLastMove(){
		return this.lastAction;
	}
	
	/**
	 * Returns the starting location
	 * @return   start location tile object
	 */
	public Tile getStartLocation(){
		return this.startLocation;
	}


	/**
	 * Clones a State object
	 */
	public State clone(){
		State clone = new State();

		clone.allTiles = new ArrayList<Tile>();
		for(Tile p : this.allTiles){
			clone.allTiles.add((Tile)p.clone());
		}
		clone.orientation = this.orientation;
		clone.startLocation = (Tile)this.startLocation.clone();
		
		clone.movesMade = this.movesMade;
		clone.axePickUp = this.axePickUp;
		clone.keyPickUp = this.keyPickUp;
		clone.goldPickUp = this.goldPickUp;
		clone.goldSeen = this.goldSeen;
		clone.axeSeen = this.goldSeen;
		clone.keySeen = this.goldSeen;
		clone.finished = this.finished;
		clone.dead = this.dead;
		clone.stonesAvailable = this.stonesAvailable;
		clone.lastAction = this.lastAction;       
		clone.previous = this.previous;

		return clone;
	}

	/**
	 * Hash code for the hashmap <br>
	 * No significance in the numbers
	 * @Override
	 */
	public int hashCode() {
		final int hash = 27;
		int result = 1;
		result = hash * result + lastAction;
		result = hash * result + (dead ? 717 : 719 );
		result = hash * result + (finished ? 717 : 719);
		result = hash * result + (axePickUp ? 717 : 719);
		result = hash * result + (goldPickUp ? 717 : 719);
		result = hash * result + (keyPickUp ? 717 : 719);

		result = hash * result + (goldSeen ? 717 : 719);
		result = hash * result + (axeSeen ? 717 : 719);
		result = hash * result + (keySeen ? 717 : 719);
		result = hash * result + stonesAvailable;
	
		result = hash * result + startLocation.getX();
		result = hash * result + startLocation.getY();
		result = hash * result + orientation;
		return result;
	}

	/**
	 * Checks whether object is equal to another
	 * @return boolean value indicating whether they are equal
	 * @Override
	 */
	public boolean equals(Object obj){
	
		if (this == obj) {
			return true;
		} else if (obj == null) {
			return false;
		} else if (getClass() != obj.getClass()) {
			return false;
		}

		State other = (State) obj;
		if (dead != other.dead) {
			return false;
		}
		if (finished != other.finished) {
			return false;
		}
		if (axePickUp != other.axePickUp) {
			return false;
		}
		if (goldPickUp != other.goldPickUp){
			return false;
		}
		if (keyPickUp != other.keyPickUp){
			return false;
		}
		if (keySeen != other.keySeen) {
			return false;
		}	
	       
		if ((lastAction == 'c') != (other.lastAction == 'c')) {
			return false;
		}
		if ((lastAction == 'u') != (other.lastAction == 'u')) {
			return false;
		}

		if (stonesAvailable != other.stonesAvailable) {
			return false;
		}
		if(orientation != other.orientation){
			return false;
		}

		Tile start = startLocation.clone();
		for(int o = orientation; o != other.orientation; o = (o+1)%4){
			start.rotateLeft();
		}

		if (!start.equals(other.startLocation)){
			return false;
		}
		return true;
	}

	private ArrayList<Tile> allTiles;
	private Tile startLocation;
	private char inFront;
	private int movesMade = 0;
	private State previous = null;
	private int orientation = NORTH;

	private boolean axePickUp  = false;
	private boolean keyPickUp  = false;
	private boolean goldPickUp = false;
	private boolean keySeen;
	private boolean axeSeen;
	private boolean goldSeen;
	
	private boolean finished  = false;
	private boolean dead = false;
	private int stonesAvailable = 0;
	private char lastAction;
	
	private static final int WINDOW_SIZE = 5;
	private static final int NORTH = 0;
	private static final int WEST = 3;
}