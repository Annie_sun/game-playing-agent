import java.util.HashMap;
import java.util.Comparator;

/**
 * Heuristic class:  <br>
 * -Deals with calculation of heuristics <br>
 * 
 * -Ideal states such as collecting gold, axe, key or cutting trees, unlocking doors
 * are given higher priority <br>
 * 
 * -If there isn't any ideal states, then the heuristic is calculated by distance between 
 * agent and the closest location of corresponding item. Then it is multiplied by a weight to 
 * differentiate to represent the different priorities of getting a certain item <br>
 * 
 * -Thus exploring the area takes precedence, followed by collecting items (stone, axe, key) and 
 * followed by gold last. The reason gold is low priority is due to the fact that most of the times,
 * the pathway to gold is usually blocked by trees, doors and water. Thus it makes no sense to try to
 * get to the gold first before collecting these essential items. Even if this is not the case, there
 * is no harm in collecting the tools on the way to the gold as the aim is not to build an optimal 
 * path but to collect the goal and return back to starting location
 *  
 * @author Annie Sun (z5075255)
 *
 */
public class Heuristic implements Comparator<State>{
	/**
	 * Constructs a Heuristic Object
	 */
	public Heuristic(){
		this.min = new HashMap<>();
		
	}

	/**
	 * Finds the heuristic value of a given state
	 * @param s   given state
	 * @return    heuristic value
	 */
	public int getHeuristicValue(State s){

		HashMap<Character, Tile> itemLocations = s.closestLocation();
		char lastMove = s.getLastMove();

		boolean hadGoldInPrev = s.getPrevious().goldPickUp();
		boolean hadAxeInPrev = s.getPrevious().getAxePickUp();
		boolean hadKeyInPrev = s.getPrevious().getKeyPickUp();
		boolean keyInFront = s.getKeyPickUp();
		boolean goldInFront = s.goldPickUp();
		boolean axeInFront = s.getAxePickUp();

		// looking for ideal states such as picking up axe, going back to start with gold picked up 

		if(!hadGoldInPrev && goldInFront){
			return 0;
			// gold has been picked up and now only thing to do is go back to starting location
		} else if(hadGoldInPrev && goldInFront){
			return s.startLocation().manDist();
		} else if(!hadKeyInPrev && keyInFront) {
			return 1;
		} else if(!hadAxeInPrev && axeInFront){
			return 1;
		} 
		
		// keep cutting or unlocking if possible
		if(lastMove == 'c') {
			return 0;       
		} else if (lastMove == 'u') {
			return 0;
		}
			
		char infront = s.getTile(0, -1).getItem(); 
		
		if (s.goldSeen() && infront == '~' && (s.getStonesAvailable() > 0)) {
			return 0;
		}
				
		if(!itemLocations.containsKey('g') && infront == '?'){
			return 0;
		}
		
		// if no ideal states, heuristic = distance to item from agent    
		int hn = 0;
		int lowestHn = NUM;

		String targetItem = "gako?";        
		int weights[] = {20,10,10,10,1};     // explanation above for the weights ^

		
		for(int i = 0; i < targetItem.length(); i++){
			char item = targetItem.charAt(i);
			if(itemLocations.containsKey(item)){
				hn = weights[i]*itemLocations.get(item).manDist();
				
				if(hn < lowestHn){
					lowestHn = hn;
				}
			}
		} 
		return lowestHn;
	}

	/**
	 * Sets the gn for a given state
	 * @param s        given state
	 * @param value    gn
	 */
	public void setMinGn(State s, int value){
		min.put(s, value);
	}

	/**
	 * Returns the gn for a given state
	 * @param s   given state
	 * @return    gn for a state
	 */
	public int getMinGn(State s){
		return this.min.get(s);
	}

	/**
	 * Returns total cost (hn + gn) of a given state
	 * @param s   given state 
	 * @return    total cost of a states
	 */
	public int getFn(State s){
		int value = 0;
		int gn = getMinGn(s);
		int hn = getHeuristicValue(s);
		value = hn + gn;
		
		return value;
	}

	/**
	 * A comparator for comparing the states by their fn value
	 */
	public int compare(State o1, State o2) {
		Integer value1 = getFn(o1);
		Integer value2 = getFn(o2);

		if(value1 == null || value2 == null){
			return 0;
		}

		if(value1 > value2){
			return 1;
		} else if(value1 < value2){
			return -1;
		}
		return 0;
	}

	private HashMap<State, Integer> min;
	private static int NUM = 10000;

}