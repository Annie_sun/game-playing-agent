import java.util.HashMap;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * AStarSearch class: <br>
 * - Responsible for implementing an a* search  <br>
 * - A detailed analysis heuristic used in the Heuristic.java file
 * - NumNodesExpanded is random num to prevent the search space to infinite search
 *  (set to 10000 since that is the max number of moves possible)
 * - Simulates and evaluates possible moves adding only those that don't lead to our death
 * 
 * @author Annie Sun (z5075255)
 */

public class AStarSearch {

	/**
	 * Constructs a AStarSearch Object
	 * @param start   given state
	 */
	public AStarSearch(State start){
		this.start = start;
		this.heuristic = new Heuristic();
		this.moves = null;
	}
		

	/**
	 * A* search process <br>
	 * -Priority Queue is sorted by heuristic value
	 */
	public void search() {
		numNodesExpanded = 0;
		moves = new ArrayList<>();
		PriorityQueue<State> pq = new PriorityQueue<>(1, heuristic); 
		HashMap<State, State> prevList = new HashMap<>();

		State curr = start;
		prevList.put(start, null);
		heuristic.setMinGn(start, 0);

		State bestChoice = null;
		int bestChoiceFn = 0;
		int fn;

		while(curr != null && numNodesExpanded < maxNodes){
			fn = heuristic.getHeuristicValue(curr);
			if(bestChoice == null || bestChoiceFn > fn){
				bestChoice = curr;
				bestChoiceFn = fn;
				moves = makePath(prevList, bestChoice);
			
				if(fn == 0){
					break;
				}
			}

			int distanceTraveled = heuristic.getMinGn(curr);
			for(State s : addStates(curr)){
				boolean toVisit = prevList.get(s) == null;
				int pathLen = distanceTraveled + 1;

				if(toVisit || pathLen < heuristic.getMinGn(s)){
					heuristic.setMinGn(s, pathLen);
					prevList.put(s, curr);
 
					if(toVisit){
						pq.add(s);
					}
				}
			}
			curr = pq.poll();
			numNodesExpanded = numNodesExpanded + 1;
		}
	}

	/**
	 * 
	 * @param prevList   hashmap of the states and their corresponding parent state
	 * @param curr       current state
	 * @return		     path from start to curr in an arraylist
	 */
	private ArrayList<State> makePath(HashMap<State, State> prevList, State curr){
		ArrayList<State> path = new ArrayList<>();
		// append to the start of the list so the list curr -> curr.prev -> start is
		// put into the right order
		while(curr != start && curr != null){
			path.add(0,curr);
			curr = prevList.get(curr);
		}
		path.add(0,curr);
	
		return path;
	}

	/**
	 * Returns an arraylist of new states that can be put into the priority queue <br>
	 * Only includes those who doesn't result in the AI's death  <br>
	 * So stepping into water without a stone is not allowed
	 * @param curr   given state
	 * @return       arrayList of new states that can be added to priority queue
	 */
	public ArrayList<State> addStates(State curr){
		ArrayList<State> nextMoves = new ArrayList<>();
		String possibleMoves = curr.possibleMoves();

		for(int index = 0; index < possibleMoves.length(); index++){
			char action = possibleMoves.charAt(index);
			State newS = curr.clone();
			boolean alive = newS.demo(action);
			newS.setPrevious(curr);

			if(alive){
				nextMoves.add(newS);
			}
		}
		return nextMoves;
	}

	/**
	 * Returns an arraylist of moves to the AI
	 * @return    arraylist of moves
	 */
	public ArrayList<State> getMoves() {
		return moves;
	}
	
	private Heuristic heuristic;
	private ArrayList<State> moves;

	private State start;
	private int numNodesExpanded;
	private int maxNodes = 10000;

}